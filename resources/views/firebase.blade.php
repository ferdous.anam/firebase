<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Phone Number Authentication with Firebase Web</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>


<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Enter number to create account</h4>
            <input type="text" id="number" name="number" class="form-control" placeholder="+880********">
            <div id="recaptcha-container"></div>
            <button type="button" onclick="phoneAuth();" class="btn btn-primary">SendCode</button>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Enter Verification code</h4>
            <input type="text" id="verificationCode" class="form-control" placeholder="Enter verification code">
            <button type="button" onclick="codeverify();" class="btn btn-primary">Verify code</button>
        </div>
    </div>
</div>


<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.21.1/firebase-app.js"></script>

<script src="https://www.gstatic.com/firebasejs/7.21.1/firebase-auth.js"></script>

<script>
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyBj-oV7qIu3w46NKSe1S2HnfguFb_I3_Io",
        authDomain: "fir-auth-1e2dc.firebaseapp.com",
        databaseURL: "https://fir-auth-1e2dc.firebaseio.com",
        projectId: "fir-auth-1e2dc",
        storageBucket: "fir-auth-1e2dc.appspot.com",
        messagingSenderId: "343798564135",
        appId: "1:343798564135:web:03a67bf439e7d43d879502",
        measurementId: "G-9CFEDSMPHK"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
</script>
<script>
    window.onload = function () {
        render();
    };

    function render() {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
        recaptchaVerifier.render();
    }

    function phoneAuth() {
        //get the number
        var number = document.getElementById('number').value;
        //phone number authentication function of firebase
        //it takes two parameter first one is number,,,second one is recaptcha
        firebase.auth().signInWithPhoneNumber(number, window.recaptchaVerifier).then(function (confirmationResult) {
            //s is in lowercase
            window.confirmationResult = confirmationResult;
            coderesult = confirmationResult;
            console.log(coderesult);
            alert("Message sent");
        }).catch(function (error) {
            alert(error.message);
        });
    }

    function codeverify() {
        var code = document.getElementById('verificationCode').value;
        coderesult.confirm(code).then(function (result) {
            alert("Successfully registered");
            var user = result.user;
            console.log(user);
        }).catch(function (error) {
            alert(error.message);
        });
    }
</script>
</body>
</html>
